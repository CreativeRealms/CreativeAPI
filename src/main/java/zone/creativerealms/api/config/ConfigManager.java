package zone.creativerealms.api.config;

import lombok.Getter;
import zone.creativerealms.api.CreativeAPI;
import zone.creativerealms.api.utils.files.FileUtils;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.*;

/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) CreativeRealms - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: zone.creativerealms.api
 * Project: CreativeAPI
 */
public class ConfigManager {

    private CreativeAPI plugin;

    //config
    private File configFile;
    private File lobbyFile;
    private File announcerFile;

    private static FileConfiguration config;

    @Getter
    private static FileConfiguration lobby;

    @Getter
    private static FileConfiguration announcer;

    public ConfigManager(CreativeAPI plugin) {
        this.plugin = plugin;
    }

    public void setup() {

        //make sure directory exists
        if (!plugin.getDataFolder().isDirectory()) {
            plugin.getDataFolder().mkdir();
        }

        //config files
        configFile = new File(plugin.getDataFolder(), "config.yml");
        lobbyFile = new File(plugin.getDataFolder(), "lobby.yml");
        announcerFile = new File(plugin.getDataFolder(), "announcer.yml");

        //setup the config manager
        if(!configFile.exists()) {
            //load the config file
            FileUtils.loadFile("config.yml");
        }

        if (!lobbyFile.exists()) {
            FileUtils.loadFile("lobby.yml");
        }

        if (!announcerFile.exists()) {
            FileUtils.loadFile("announcer.yml");
        }

        //attempt to load the file configurations
        reloadConfig();
        reloadLobby();
        reloadAnnouncer();

    }

    public void reloadConfig() {
        try {
            config = new YamlConfiguration();
            config.load(configFile);
        } catch (Exception ex) {
            plugin.getLogger().severe("Could not load config: " + configFile.getName());
        }
    }

    public void reloadLobby() {
        try {
            lobby = new YamlConfiguration();
            lobby.load(lobbyFile);
        } catch (Exception ex) {
            plugin.getLogger().severe("Could not load config: " + lobbyFile.getName());
        }
    }

    public void reloadAnnouncer() {
        try {
            announcer = new YamlConfiguration();
            announcer.load(announcerFile);
        } catch (Exception ex) {
            plugin.getLogger().severe("Could not load confi: " + announcerFile.getName());
        }
    }

    public void saveConfig() {
        try {
            config.save(configFile);
        } catch (IOException ex) {
            plugin.getLogger().severe("Could not save config: " + configFile.getName());
        }
    }

    public void saveLobby() {
        try {
            lobby.save(lobbyFile);
        } catch (IOException ex) {
            plugin.getLogger().severe("Could not save config: " + lobbyFile.getName());
        }
    }

    public void saveAnnouncer() {
        try {
            announcer.save(announcerFile);
        } catch (IOException ex) {
            plugin.getLogger().severe("Could not save config: " + announcerFile.getName());
        }
    }

    public static FileConfiguration getConfig() {
        return config;
    }

}