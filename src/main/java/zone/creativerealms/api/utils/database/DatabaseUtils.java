package zone.creativerealms.api.utils.database;

import zone.creativerealms.api.CreativeAPI;
import zone.creativerealms.api.sql.SQLUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) CreativeRealms - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: zone.creativerealms.api
 * Project: CreativeAPI
 */
public class DatabaseUtils {

    public static void loadTables() {

        try {
            if (!CreativeAPI.getDB().checkConnection()) {
                return;
            }
        } catch (Exception ignore) {}

        try {
            loadLobbyServersTable();
        } catch (Exception ex) {
            CreativeAPI.getInstance().getLogger().log(Level.SEVERE, "Could not create lobby_servers table", ex);
            CreativeAPI.getInstance().endSetup("Could not create lobby_servers table");
        }

        try {
            loadCreativeServersTable();
        } catch (Exception ex) {
            CreativeAPI.getInstance().getLogger().log(Level.SEVERE, "Could not create creative_servers table", ex);
            CreativeAPI.getInstance().endSetup("Could not create creative_servers table");
        }

        try {
            loadUsersTable();
        } catch (Exception ex) {
            CreativeAPI.getInstance().getLogger().log(Level.SEVERE, "Could not create users table", ex);
            CreativeAPI.getInstance().endSetup("Could not create users table");
        }


    }


    public static void loadLobbyServersTable() throws SQLException, ClassNotFoundException {
        //check if the playerdata table exists
        if (!SQLUtil.tableExists(CreativeAPI.getDB(), "lobby_servers")) {

            //create the playerdata table
            CreativeAPI.getInstance().getLogger().log(Level.INFO, "lobby_servers table does not exist, creating...");
            CreativeAPI.getDB().executeSQL(
                    "CREATE TABLE IF NOT EXISTS `lobby_servers` (" +
                            "`id` BIGINT NOT NULL AUTO_INCREMENT, " +
                            "`uuid` varchar(64) NOT NULL," +
                            "`name` varchar(32) DEFAULT NULL," +
                            "`address` varchar(128) DEFAULT NULL," +
                            "`region` TEXT DEFAULT NULL," +
                            "`state` TEXT DEFAULT NULL," +
                            "`count` int(11) DEFAULT NULL," +
                            "`max` int(11) DEFAULT NULL," +
                            "PRIMARY KEY (`id`)," +
                            "KEY `uuid_index` (`uuid`)" +
                            ") ENGINE = InnoDB;");

            //log successful creation
            CreativeAPI.getInstance().getLogger().log(Level.INFO, "lobby_servers table created successfully!");
        }
    }

    public static void loadCreativeServersTable() throws SQLException, ClassNotFoundException {
        //check if the playerdata table exists
        if (!SQLUtil.tableExists(CreativeAPI.getDB(), "creative_servers")) {

            //create the playerdata table
            CreativeAPI.getInstance().getLogger().log(Level.INFO, "creative_servers table does not exist, creating...");
            CreativeAPI.getDB().executeSQL(
                    "CREATE TABLE IF NOT EXISTS `creative_servers` (" +
                            "`id` BIGINT NOT NULL AUTO_INCREMENT, " +
                            "`uuid` varchar(64) NOT NULL," +
                            "`name` varchar(32) DEFAULT NULL," +
                            "`address` varchar(128) DEFAULT NULL," +
                            "`region` TEXT DEFAULT NULL," +
                            "`state` TEXT DEFAULT NULL," +
                            "`count` int(11) DEFAULT NULL," +
                            "`max` int(11) DEFAULT NULL," +
                            "`plot_type` TEXT DEFAULT NULL," +
                            "PRIMARY KEY (`id`)," +
                            "KEY `uuid_index` (`uuid`)" +
                            ") ENGINE = InnoDB;");

            //log successful creation
            CreativeAPI.getInstance().getLogger().log(Level.INFO, "creative_servers table created successfully!");
        }
    }

    public static void loadUsersTable() throws SQLException, ClassNotFoundException {

        //check if the users table exists
        if (!SQLUtil.tableExists(CreativeAPI.getDB(), "users")) {

            //create the users table
            CreativeAPI.getInstance().getLogger().log(Level.INFO, "users table does not exist, creating...");

            CreativeAPI.getDB().executeSQL(
                    "CREATE TABLE IF NOT EXISTS `users` (" +
                            "`id` BIGINT NOT NULL AUTO_INCREMENT, " +
                            "`uuid` varchar(64) NOT NULL," +
                            "`name` varchar(32) DEFAULT NULL," +
                            "`rank` TEXT DEFAULT NULL," +
                            "PRIMARY KEY (`id`)," +
                            "KEY `uuid_index` (`uuid`)" +
                            ") ENGINE = InnoDB;");

            //log successful creation
            CreativeAPI.getInstance().getLogger().log(Level.INFO, "users table created successfully!");
        }

    }


}
