package zone.creativerealms.api.utils;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) CreativeRealms - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: zone.creativerealms.api
 * Project: CreativeAPI
 */
@SuppressWarnings("deprecation")
public class PlayerUtils {

    /**
     * Gets an offline player by a name.
     * <p>
     * This is just because of the deprecation on <code>Bukkit.getOfflinePlayer(String)</code>
     *
     * @param name The name.
     * @return the offline player.
     */
    public static OfflinePlayer getOfflinePlayer(String name) {
        return Bukkit.getOfflinePlayer(name);
    }

    /**
     * Broadcasts a message to everyone online.
     *
     * @param message the message.
     */
    public static void broadcast(String message) {
        broadcast(message, null);
    }

    /**
     * Broadcasts a message to everyone online with a specific permission.
     *
     * @param message the message.
     * @param permission the permission.
     */
    public static void broadcast(String message, String permission) {
        for (Player online : Bukkit.getOnlinePlayers()) {
            if (permission != null && !online.hasPermission(permission)) {
                continue;
            }

            online.sendMessage(message);
        }

        message = message.replaceAll("§l", "");
        message = message.replaceAll("§o", "");
        message = message.replaceAll("§r", "§f");
        message = message.replaceAll("§m", "");
        message = message.replaceAll("§n", "");

        Bukkit.getLogger().info(message);
    }
    
    /**
     * Broadcast the given message to all players online with staff perms except the given sender.
     */
    public static void broadcastCmdInfo(CommandSender sender, String message) {
        for (Player online : Bukkit.getOnlinePlayers()) {
            if (online == sender) {
                continue;
            }
            
            if (!online.hasPermission("sparkuhc.staff")) {
                continue;
            }

            online.sendMessage("§7§o[" + sender.getName() + ": " + message + "]");
        }

        if (!(sender instanceof Player)) {
            return;
        }
        
        Bukkit.getLogger().info("§7[" + sender.getName() + ": " + message + "]");
    }

}