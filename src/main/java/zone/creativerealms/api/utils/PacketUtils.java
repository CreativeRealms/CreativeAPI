package zone.creativerealms.api.utils;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import zone.creativerealms.api.CreativeAPI;
import zone.creativerealms.api.config.ConfigManager;
import zone.creativerealms.api.user.User;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import zone.creativerealms.api.utils.communication.ChatUtils;

import java.util.logging.Level;

/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) CreativeRealms - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: zone.creativerealms.api
 * Project: CreativeAPI
 */
public class PacketUtils {

    public static void setup() {

        CreativeAPI.getInstance().getLogger().info("Setting up header update task...");

        BukkitRunnable headerUpdater = new BukkitRunnable() {
            @Override
            public void run() {

                for (User user : CreativeAPI.getInstance().getUserManager().getUsers()) {
                    updaterHeader(user);
                }

            }
        };


        headerUpdater.runTaskTimer(CreativeAPI.getInstance(), 0, 10);

    }

    public static void updaterHeader(User user){
        String header = ChatColor.translateAlternateColorCodes('&', ConfigManager.getConfig().getString("tab.title")) +  "\n"
                + ChatColor.GRAY + ">" + ChatColor.STRIKETHROUGH.toString() + ChatUtils.repeat(50, " ") + ChatColor.GRAY + "<";
        String footer = ChatColor.BLUE + "Players: " + ChatColor.WHITE + Bukkit.getOnlinePlayers().size() + "/" + Bukkit.getServer().getMaxPlayers() +
                "\n" + ChatColor.GRAY + ">" + ChatColor.STRIKETHROUGH.toString() + ChatUtils.repeat(50, " ") + ChatColor.GRAY + "<";
        user.setHeaderFooter(header, footer);
    }

    public static void sendAction(Player player, String message) {
        final PacketContainer title = ProtocolLibrary.getProtocolManager().createPacket(PacketType.Play.Server.CHAT);
        title.getChatComponents().write(0, WrappedChatComponent.fromText(message));
        title.getBytes().write(0, (byte) 2);
        try {
            ProtocolLibrary.getProtocolManager().sendServerPacket(player, title);
        } catch (Throwable e1) {
            CreativeAPI.getInstance().getLogger().log(Level.SEVERE, "Could not send tab packet", e1);
        }
    }

}
