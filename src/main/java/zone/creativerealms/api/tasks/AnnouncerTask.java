package zone.creativerealms.api.tasks;

import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.scheduler.BukkitRunnable;
import zone.creativerealms.api.CreativeAPI;
import zone.creativerealms.api.message.MessageManager;
import zone.creativerealms.api.message.PrefixType;
import zone.creativerealms.api.utils.PlayerUtils;
import zone.creativerealms.api.utils.communication.ChatUtils;

import java.util.Random;

/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) CreativeRealms - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: zone.creativerealms.api
 * Project: CreativeAPI
 */
public class AnnouncerTask extends BukkitRunnable{

    //CreativeAPI instance
    private CreativeAPI plugin;
    private final Random rand = new Random();

    public AnnouncerTask(CreativeAPI plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {

        if (!plugin.getAnnouncer().isEnabled()) {
            return;
        }

        PlayerUtils.broadcast(ChatColor.GRAY + ">" + ChatColor.STRIKETHROUGH.toString() + ChatUtils.repeat(100, " ") + ChatColor.GRAY + "<");

        MessageManager.broadcastPrefixMessage(PrefixType.MAIN,
                plugin.getAnnouncer().getAnnouncements().get(rand.nextInt(plugin.getAnnouncer().getAnnouncements().size())));

        MessageManager.broadcastSound(Sound.BLOCK_NOTE_BASS);

        PlayerUtils.broadcast(ChatColor.GRAY + ">" + ChatColor.STRIKETHROUGH.toString() + ChatUtils.repeat(100, " ") + ChatColor.GRAY + "<");

    }

}
