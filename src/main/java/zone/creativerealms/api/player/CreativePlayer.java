package zone.creativerealms.api.player;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) CreativeRealms - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: zone.creativerealms.api
 * Project: CreativeAPI
 */
public class CreativePlayer {

    private UUID uuid;
    private Player player;
    private Rank rank;

    private String name;
    private boolean initialized = false;
    private List<String> ips = new ArrayList<>();
    private String firstJoined;
    private ChatColor nameColor = ChatColor.BLUE;

    //staff mode
    private boolean staffMode = false;

    public CreativePlayer(UUID uuid) {
        this.uuid = uuid;
        this.player = Bukkit.getPlayer(uuid);
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIps(List<String> ips) {
        this.ips = ips;
    }

    public void setFirstJoined(String firstJoined) {
        this.firstJoined = firstJoined;
    }

    public void setRank(Rank rank) {
        this.rank = rank;

        //todo: manually set perms for staff, and donors, and regulars
        /*if (rank.has(null, Rank.OWNER, false)) {

        }*/
    }

    public void setInitialized(boolean initialized) {
        this.initialized = initialized;
    }

    public void setStaffMode(boolean enable) {
        this.staffMode = enable;
    }

    public void setNameColor(ChatColor color) {
        this.nameColor = color;
    }

    public UUID getUUID() {
        return uuid;
    }

    public Player getPlayer() {
        return player;
    }

    public String getName() {
        return name;
    }

    public Rank getRank() {
        return rank;
    }

    public String getPrefix() {
        return getRank().equals(Rank.DEFAULT) ? "" : ChatColor.DARK_GRAY + "[" + getRank().getTag() + ChatColor.DARK_GRAY + "]";
    }

    public boolean isInitialized() {
        return initialized;
    }

    public List<String> getIps() {
        return ips;
    }

    public String getFirstJoined() {
        return firstJoined;
    }

    public boolean isStaffModeEnabled() {
        return staffMode;
    }

    public ChatColor getNameColor() {
        return nameColor;
    }

}
