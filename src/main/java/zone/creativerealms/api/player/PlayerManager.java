package zone.creativerealms.api.player;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import zone.creativerealms.api.CreativeAPI;
import zone.creativerealms.api.message.MessageManager;
import zone.creativerealms.api.permissions.PermissionManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) CreativeRealms - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: zone.creativerealms.api
 * Project: CreativeAPI
 */
public class PlayerManager implements Listener {

    private CreativeAPI plugin;
    private static ArrayList<CreativePlayer> creativePlayers = new ArrayList<>();

    public PlayerManager(CreativeAPI plugin) {
        this.plugin = plugin;
        CreativeAPI.registerListener(this);
    }

    public static CreativePlayer getCreativePlayer(UUID uuid) {
        for (CreativePlayer creativePlayer : creativePlayers) {
            if (creativePlayer.getUUID().equals(uuid)) {
                return creativePlayer;
            }
        }
        return getSQLCreativePlayer(uuid);
    }

    private static CreativePlayer getSQLCreativePlayer(UUID uuid) {
        try {
            PreparedStatement stmt = CreativeAPI.getDB().getConnection().prepareStatement("SELECT * FROM users WHERE uuid = ? LIMIT 1;");
            stmt.setString(1, uuid.toString());
            ResultSet set = stmt.executeQuery();
            if (set.next()) {

                CreativePlayer creativePlayer = new CreativePlayer(uuid);

                creativePlayer.setName(creativePlayer.getPlayer().getName());
                creativePlayer.setRank(Rank.valueOf(set.getString("rank")));


                return creativePlayer;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void updateCreativePlayer(CreativePlayer creativePlayer, String key, String value) {
        try {
            PreparedStatement stmt = CreativeAPI.getDB().getConnection().prepareStatement("UPDATE users SET ? = ? WHERE uuid = ?;");
            stmt.setString(1, key);
            stmt.setString(2, value);
            stmt.setString(3, creativePlayer.getUUID().toString());
            stmt.execute();
            stmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void globalPlayerSync(CreativePlayer creativePlayer) {

        boolean newPlayer;
        String uuid = creativePlayer.getUUID().toString();


        try {
            PreparedStatement stmt = CreativeAPI.getDB().getConnection().prepareStatement("SELECT * FROM users WHERE uuid = ? LIMIT 1;");
            stmt.setString(1, uuid);
            ResultSet set = stmt.executeQuery();

            if (set.next()) {
                stmt.close();
                newPlayer = false;

                //update name
                stmt = CreativeAPI.getDB().getConnection().prepareStatement("UPDATE users SET name = ? WHERE uuid = ?;");
                stmt.setString(1, creativePlayer.getPlayer().getName());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

                //update rank
                stmt = CreativeAPI.getDB().getConnection().prepareStatement("UPDATE users SET rank = ? WHERE uuid = ?;");
                stmt.setString(1, creativePlayer.getRank().toString());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

            } else {
                newPlayer = true;

                stmt.close();
                stmt = CreativeAPI.getDB().getConnection().prepareStatement("INSERT INTO users (uuid, name, rank) VALUES (?, ?, ?);");
                stmt.setString(1, uuid);

                //set the players name
                stmt.setString(2, creativePlayer.getPlayer().getName());
                creativePlayer.setName(creativePlayer.getPlayer().getName());

                //set player to default rank
                stmt.setString(3, Rank.DEFAULT.toString());
                creativePlayer.setRank(Rank.DEFAULT);

                //todo: join date, etc
                stmt.execute();
                stmt.close();

            }

            creativePlayer.setInitialized(true);

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onLogin(PlayerJoinEvent e) {
        Player player = e.getPlayer();
        CreativePlayer creativePlayer = getCreativePlayer(player.getUniqueId());

        if (creativePlayer == null) {
            creativePlayer = new CreativePlayer(player.getUniqueId());
            globalPlayerSync(creativePlayer);
        }

        //give the player permissions
        PermissionManager.addPerms(creativePlayer);

        creativePlayers.add(creativePlayer);

        MessageManager.getAnnounceBar().addPlayer(player);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onQuit(PlayerQuitEvent e) {
        Player player = e.getPlayer();
        handleLeave(player);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onKick(PlayerKickEvent e) {
        Player player = e.getPlayer();
        handleLeave(player);
    }

    private void handleLeave(Player player) {
        CreativePlayer creativePlayer = getCreativePlayer(player.getUniqueId());

        //remove perms from player
        PermissionManager.removePerms(creativePlayer);

        //remove player form list
        creativePlayers.remove(creativePlayer);
        MessageManager.getAnnounceBar().removePlayer(player);
    }

    public static ArrayList<CreativePlayer> getCreativePlayers() {
        return creativePlayers;
    }

}
