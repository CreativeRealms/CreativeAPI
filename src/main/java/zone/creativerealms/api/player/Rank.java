package zone.creativerealms.api.player;


import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.entity.Player;
import zone.creativerealms.api.message.MessageManager;
import zone.creativerealms.api.message.PrefixType;

/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) CreativeRealms - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: zone.creativerealms.api
 * Project: CreativeAPI
 */
public enum Rank {

    OWNER("Owner", ChatColor.DARK_RED, 15),
    ADMIN("Admin", ChatColor.DARK_RED, 14),
    MANAGER("Manager", ChatColor.LIGHT_PURPLE, 13),
    HOST("Host", ChatColor.DARK_PURPLE, 12),
    SRMOD("Sr.Mod", ChatColor.GOLD, 11),
    MOD("Mod", ChatColor.GOLD, 10),
    TRIAL("Trial", ChatColor.DARK_GREEN, 9),
    BUILDER("Builder", ChatColor.BLUE, 8),
    DEV("Dev", ChatColor.RED, 7),
    TWITCH("Twitch", ChatColor.BLUE, 6),
    YOUTUBE("Youtube", ChatColor.BLUE, 5),
    MVP_PLUS("MVP+", ChatColor.AQUA, 4),
    MVP("MVP", ChatColor.AQUA, 3),
    VIP_PLUS("VIP+", ChatColor.GREEN, 2),
    VIP("VIP", ChatColor.GREEN, 1),
    DEFAULT("", ChatColor.WHITE, 0);

    private String name;
    private ChatColor tagColor;
    private int level;

    private Rank(String name, ChatColor tagColor, int level) {
        this.name = name;
        this.tagColor = tagColor;
        this.level = level;
    }

    public boolean has(Player player, Rank rank, boolean inform) {
        if (compareTo(rank) >= 7) {
            return true;
        }

        if (inform) {
            player.sendMessage(MessageManager.getPrefix(PrefixType.PERMISSIONS) + "This requires Permission Rank " + rank.getTag());
        }

        return false;
    }

    public String getName() {
        return name;
    }

    public ChatColor getTagColor() {
        return tagColor;
    }

    /**
     * Get the level of the rank.
     * <p>
     * It goes in order from 0 to 15 with 15 being the highest rank and 0 being the lowest.
     *
     * @return The level.
     */
    public int getLevel() {
        return level;
    }

    public String getTag() {
        return getTagColor() + getName();
    }

}
