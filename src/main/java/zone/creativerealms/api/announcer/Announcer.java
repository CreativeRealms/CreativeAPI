package zone.creativerealms.api.announcer;

import zone.creativerealms.api.CreativeAPI;
import zone.creativerealms.api.config.ConfigManager;
import zone.creativerealms.api.tasks.AnnouncerTask;

import java.util.List;
import java.util.Random;

/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) CreativeRealms - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: zone.creativerealms.api
 * Project: CreativeAPI
 */
public class Announcer {

    //CreativeAPI instance
    private CreativeAPI plugin;

    private boolean enabled;
    private List<String> announcements;
    private AnnouncerTask announcerTask;

    public Announcer(CreativeAPI plugin) {
        this.plugin = plugin;
        announcerTask = new AnnouncerTask(plugin);
    }

    public void setup() {

        loadSettings();
        announcerTask.runTaskTimer(plugin, 10000,10000);

    }

    public void reload() {

        announcements.clear();
        loadSettings();
    }

    private void loadSettings() {
        try {
            enabled = ConfigManager.getAnnouncer().getBoolean("enabled");
            announcements = ConfigManager.getAnnouncer().getStringList("announcements");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public List<String> getAnnouncements() {
        return announcements;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}
