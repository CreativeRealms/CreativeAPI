package zone.creativerealms.api.chat;

import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) CreativeRealms - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: zone.creativerealms.api
 * Project: CreativeAPI
 */
public class ChatChannel {

    private String name;
    private String channelPrefix;
    private boolean filter = false;
    private boolean muted = false;
    private ArrayList<String> wordFilter = new ArrayList<>();

    public ChatChannel(String name, String channelPrefixl) {

        this.name = name;
        this.channelPrefix = channelPrefix;

    }

    public String getName() {
        return name;
    }

    public String getChannelPrefix() {
        return channelPrefix;
    }

    public boolean isFilterEnabled() {
        return filter;
    }

    public boolean isMuted() {
        return muted;
    }

    public void setFilter(boolean enable) {
        this.filter = enable;
    }

    public void setMuted(boolean mute) {
        this.muted = mute;
    }

    public ArrayList<String> getWordFilter() {
        return wordFilter;
    }

    public String FilterChat(String msg) {
        int t = 0;
        for(String s : wordFilter) {
            t = 0;
            String[] pparse = new String[2];
            pparse[0] = " ";
            pparse[1] = " ";
            StringTokenizer st = new StringTokenizer(s, ",");
            while(st.hasMoreTokens()) {
                if(t < 2) {
                    pparse[t++] = st.nextToken();
                }
            }
            msg = msg.replaceAll("(?i)" + pparse[0], pparse[1]);
        }
        return msg;
    }


}
