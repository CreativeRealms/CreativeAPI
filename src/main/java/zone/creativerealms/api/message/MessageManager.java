package zone.creativerealms.api.message;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import zone.creativerealms.api.CreativeAPI;

import java.util.HashMap;
import java.util.logging.Level;

/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) CreativeRealms - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: zone.creativerealms.api
 * Project: CreativeAPI
 */
public class MessageManager {

    private static MessageManager instance;
    private static HashMap<PrefixType, String> prefix = new HashMap<>();
    private static BossBar announceBar;
    private static BukkitRunnable announceBarCallBack = null;

    public static MessageManager getInstance() {
        if (instance == null) {
            instance = new MessageManager();
        }
        return instance;
    }

    public void setup() {
        prefix.put(PrefixType.MAIN, ChatColor.DARK_PURPLE + "Creative" + ChatColor.DARK_AQUA + "Realms" + " " + ChatColor.DARK_GRAY + "»");
        prefix.put(PrefixType.ARROW, ChatColor.DARK_GRAY + "»");
        prefix.put(PrefixType.PERMISSIONS, ChatColor.BLUE + "Permissions" + " " + ChatColor.DARK_GRAY + "»");
        prefix.put(PrefixType.JOIN, ChatColor.BLUE + "Join" + " " + ChatColor.DARK_GRAY + "»");
        prefix.put(PrefixType.QUIT, ChatColor.BLUE + "Quit" + " " + ChatColor.DARK_GRAY + "»");

        CreativeAPI.getInstance().getLogger().log(Level.INFO, "Loading announce bar...");

        announceBar = Bukkit.createBossBar("", BarColor.BLUE, BarStyle.SOLID);
        announceBar.setVisible(false);

    }

    public static String getPrefix(PrefixType type) {
        return prefix.get(type) + " " + ChatColor.RESET;
    }

    /**
     * SendMessage
     *
     * Sends a pre formated message from the plugin to a player, adding correct prefix first
     *
     * @param type the prefix type
     * @param msg the message
     * @param sender the sender
     */

    public static void sendPrefixMessage(PrefixType type, String msg, CommandSender sender){
        sender.sendMessage(getPrefix(type) + msg );
    }

    /**
     * Broadcasts a prefixed message to those with the valid permission
     *
     * @param type the prefix type
     * @param msg the message
     * @param permission the permission to check for when broadcasting the message
     */
    public static void broadcastPrefixMessage(PrefixType type, String msg, String permission) {
        for (Player online : Bukkit.getOnlinePlayers()) {
            if (permission != null && !online.hasPermission(permission)) {
                continue;
            }

            online.sendMessage(getPrefix(type) + msg);
        }

        msg = msg.replaceAll("§l", "");
        msg = msg.replaceAll("§o", "");
        msg = msg.replaceAll("§r", "§f");
        msg = msg.replaceAll("§m", "");
        msg = msg.replaceAll("§n", "");

        CreativeAPI.getInstance().getLogger().log(Level.INFO, msg);
    }

    /**
     * Broadcasts a prefixed message to the whole server
     *
     * @param type the prefix type
     * @param msg the message
     */
    public static void broadcastPrefixMessage(PrefixType type, String msg) {
        broadcastPrefixMessage(type, msg, null);
    }


    /**
     * Broadcasts an announcer bar with a prefix
     *
     * @param type the prefix type
     * @param barColor the color of the announcer bar
     * @param string the message
     * @param replace the params to replace in the message
     */
    public static void broadcastBarPrefix(PrefixType type, BarColor barColor, String string, Object ... replace){
        announceBar.setTitle(ChatColor.BOLD + getPrefix(type) + String.format(string, replace));
        if(announceBarCallBack != null){
            announceBarCallBack.cancel();
        }
        announceBar.setProgress(0);
        announceBar.setVisible(true);
        announceBar.setColor(barColor);

        announceBarCallBack = new BukkitRunnable() {
            final double num = 0.025;
            double sofar = 0;
            public void run() {
                if((sofar += num) > 1){
                    cancel();
                    announceBar.setVisible(false);
                    announceBarCallBack = null;
                }
                else announceBar.setProgress(sofar);
            }
        };
        announceBarCallBack.runTaskTimerAsynchronously(CreativeAPI.getInstance(), 0, 1);
    }

    /**
     * Broadcasts a sound to all players
     *
     * @param sound the sound to broadcast
     */
    public static void broadcastSound(Sound sound){
        for(Player player: Bukkit.getOnlinePlayers()){
            player.playSound(player.getLocation(), sound, 1f, 1f);
        }
    }

    public static BossBar getAnnounceBar() {
        return announceBar;
    }

    public static BukkitRunnable getAnnounceBarCallBack() {
        return announceBarCallBack;
    }

    public static void setAnnounceBarCallBack(BukkitRunnable callBack) {
        announceBarCallBack = callBack;
    }

}
