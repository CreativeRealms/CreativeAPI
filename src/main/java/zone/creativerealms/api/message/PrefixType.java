package zone.creativerealms.api.message;

/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) CreativeRealms - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: zone.creativerealms.api
 * Project: CreativeAPI
 */
public enum PrefixType {

    ARROW,
    MAIN,
    PERMISSIONS,
    JOIN,
    QUIT

}
