package zone.creativerealms.api;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import zone.creativerealms.api.announcer.Announcer;
import zone.creativerealms.api.commands.CommandHandler;
import zone.creativerealms.api.config.ConfigManager;
import zone.creativerealms.api.listeners.ChatListener;
import zone.creativerealms.api.listeners.JoinListener;
import zone.creativerealms.api.listeners.WorldListener;
import zone.creativerealms.api.message.MessageManager;
import zone.creativerealms.api.player.PlayerManager;
import zone.creativerealms.api.server.Region;
import zone.creativerealms.api.server.ServerType;
import zone.creativerealms.api.sql.SQLConnection;
import zone.creativerealms.api.utils.files.PropertiesFile;
import zone.creativerealms.api.user.UserManager;
import zone.creativerealms.api.utils.PacketUtils;
import zone.creativerealms.api.utils.database.DatabaseUtils;

import javax.xml.bind.Marshaller;
import java.io.File;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.UUID;
import java.util.logging.Level;

/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) CreativeRealms - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: zone.creativerealms.api
 * Project: CreativeAPI
 */
public class CreativeAPI extends JavaPlugin {


    private static boolean shutdown = false;

    private static CreativeAPI instance;
    private final File sqlpropertiesfile = new File("connect.properties");
    private PropertiesFile propertiesFile;
    private File networkFile = new File("network.properties");
    private PropertiesFile networkProperties;
    private static SQLConnection db;

    //server data
    private static UUID serverID;
    private static ServerType serverType;

    //users data folder
    private File userFolder;

    //Managers
    private ConfigManager configManager;
    private UserManager userManager;
    private PlayerManager playerManager;
    private CommandHandler commandHandler;
    private Announcer announcer;

    //Listeners
    private ChatListener chatListener;
    private JoinListener joinListener;
    private WorldListener worldListener;

    @Getter
    private String channel = "BungeeCord";

    @Getter
    private String subChannel = "CreativeRealms";

    public final void onEnable() {

        instance = this;

        getLogger().log(Level.INFO, "Loading Database tables...");
        DatabaseUtils.loadTables();

        //load our managers
        configManager = new ConfigManager(this);
        configManager.setup();
        MessageManager.getInstance().setup();
        playerManager = new PlayerManager(this);
        userManager = new UserManager(this);
        commandHandler = new CommandHandler(this);
        announcer = new Announcer(this);
        announcer.setup();

        //check for existence of user folder
        userFolder = new File(getDataFolder() + File.separator + "users" + File.separator);
        if (!userFolder.exists()) {
            userFolder.mkdir();
        }

        //register listeners
        registerListeners(getServer().getPluginManager());

        //register the commands
        commandHandler.registerCommands();

        //setup chat
        chatListener.setChatEnabled(ConfigManager.getConfig().getBoolean("chat.enabled"));

        //add all users currently online to the user map
        for (Player online : Bukkit.getOnlinePlayers()) {
            userManager.procesJoin(online);
        }

        //start any tasks we need
        PacketUtils.setup();

        //register bungeecord
        getServer().getMessenger().registerOutgoingPluginChannel(this, channel);

    }

    public final void onLoad() {

        //Loading properties
        getLogger().log(Level.INFO, "Loading SQL Properties");

        if (!sqlpropertiesfile.exists()) {
            try {
                PropertiesFile.generateFresh(sqlpropertiesfile, new String[]{"hostname", "port", "username", "password", "database"}, new String[]{"localhost", "3306", "root", "NONE", "crnetwork"});
            } catch (Exception e) {
                getLogger().log(Level.WARNING, "Could not generate fresh properties file");
            }
        }

        try {
            propertiesFile = new PropertiesFile(sqlpropertiesfile);
        } catch (Exception e) {
            getLogger().log(Level.SEVERE, "Could not load SQL properties file", e);
            endSetup("Exception occurred when loading properties");
        }

        getLogger().log(Level.INFO, "Loading Network Properties...");

        if (!networkFile.exists()) {
            try {
                PropertiesFile.generateFresh(networkFile, new String[]{"server-uuid", "server-name", "server-type", "server-region"},new String[]{UUID.randomUUID().toString(), "NALobby-1", ServerType.NONE.toString(), Region.NA.toString()});
            } catch (Exception ex) {
                getLogger().log(Level.WARNING, "Could not generate fresh properties file");
            }
        }

        //load network properties
        try {
            networkProperties = new PropertiesFile(networkFile);
        } catch (Exception e) {
            getLogger().log(Level.SEVERE, "Could not load Network Properties file", e);
            endSetup("Exception occurred when loading Network Properties");
        }

        String temp;
        getLogger().log(Level.INFO, "Finding database information...");

        //SQL info
        try {
            db = new SQLConnection(propertiesFile.getString("hostname"), propertiesFile.getNumber("port").intValue(), propertiesFile.getString("database"), propertiesFile.getString("username"), (temp = propertiesFile.getString("password")).equals("NONE") ? null : temp);
        } catch (ParseException ex) {
            getLogger().log(Level.WARNING, "Could not load database information", ex);
            endSetup("Invalid database port");
        } catch (Exception ex) {
            getLogger().log(Level.WARNING, "Could not load database information", ex);
            endSetup("Invalid configuration");
        }

        //Connecting to MySQL
        getLogger().log(Level.INFO, "Connecting to MySQL...");

        try {
            getDB().openConnection();
        } catch (Exception e) {
            getLogger().log(Level.WARNING, "Could not connect to MySQL", e);
            endSetup("Could not establish connection to database");
        }

        //attempt to get server uuid
        try {
            serverID = UUID.fromString(getNetworkProperties().getString("server-uuid"));
        } catch (Exception ex) {
            getLogger().log(Level.WARNING, "Could not load server uuid", ex);
            endSetup("Could not load server uuid.");
        }

        //attempt to set server type
        try {
            serverType = ServerType.valueOf(getNetworkProperties().getString("server-type"));
        } catch (Exception ex) {
            getLogger().severe("Could not determine server type");
            endSetup("Could not determine server type");
        }

    }

    public final void onDisable() {
        getLogger().log(Level.INFO, "Plugin is now disabling...");

        if (!shutdown) {
            //disable needed stuff
            MessageManager.getAnnounceBar().setVisible(false);
            MessageManager.getAnnounceBar().removeAll();
            userManager.getUserMap().clear();
        }

        getLogger().log(Level.INFO, "Closing connection to database...");

        try {
            getDB().closeConnection();
        } catch (SQLException e) {
            getLogger().log(Level.SEVERE, "Could not close database connection", e);
        }

    }

    public static SQLConnection getDB() {
        return db;
    }

    public PropertiesFile getProperties() {
        return propertiesFile;
    }

    public PropertiesFile getNetworkProperties() {
        return networkProperties;
    }

    public void endSetup(String s) {
        getLogger().log(Level.SEVERE, s);
        if (!shutdown) {
            stop();
            shutdown = true;
        }
        throw new IllegalArgumentException("Disabling... " + s);
    }

    private void stop() {
        Bukkit.getServer().shutdown();
    }

    public static UUID getServerID() {
        return serverID;
    }

    private void registerListeners(PluginManager pluginManager) {

        chatListener = new ChatListener(this);
        joinListener = new JoinListener(this);
        worldListener = new WorldListener(this);

        pluginManager.registerEvents(chatListener, this);
        pluginManager.registerEvents(joinListener, this);
        pluginManager.registerEvents(worldListener, this);
    }

    public static void registerListener(Listener listener) {
        Bukkit.getPluginManager().registerEvents(listener, instance);
    }

    public File getUserFolder() {
        return userFolder;
    }

    public ConfigManager getConfigManager() {
        return configManager;
    }

    public UserManager getUserManager() {
        return userManager;
    }

    public ChatListener getChatListener() {
        return chatListener;
    }

    public static CreativeAPI getInstance() {
        return instance;
    }

    public static ServerType getServerType() {
        return serverType;
    }

    public Announcer getAnnouncer() {
        return announcer;
    }

    public void setServerType(ServerType type) {
        serverType = type;
        //save to properties

        try {
            PropertiesFile.update(networkFile, "server-type", type.toString());
        } catch (Exception ex) {
            getLogger().severe("Could not update server type");
        }
    }

    public String getAddress() {
        return getServer().getIp() +":"+ getServer().getPort();
    }

    public File getNetworkFile() {
        return networkFile;
    }
}
