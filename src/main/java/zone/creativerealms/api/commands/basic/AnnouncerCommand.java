package zone.creativerealms.api.commands.basic;

import com.google.common.base.Joiner;
import zone.creativerealms.api.commands.AbstractCommand;
import zone.creativerealms.api.config.ConfigManager;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandSender;
import zone.creativerealms.api.message.MessageManager;
import zone.creativerealms.api.message.PrefixType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) CreativeRealms - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: zone.creativerealms.api
 * Project: CreativeAPI
 */
public class AnnouncerCommand extends AbstractCommand {

    public AnnouncerCommand() {
        super("announcer", "add <message>, remove <announcement>, list, enabled");
    }

    @Override
    public boolean execute(final CommandSender sender, final String[] args) throws CommandException {
        if (args.length == 0) {
            return false;
        }

        if (args[0].equalsIgnoreCase("add")) {
            if (args.length == 1) {
                throw new CommandException("Usage: /announcer add <message>");
            }

            String message = Joiner.on(' ').join(Arrays.copyOfRange(args, 1, args.length));

            //add the announcement
            plugin.getAnnouncer().getAnnouncements().add(message);
            ConfigManager.getAnnouncer().set("announcements", plugin.getAnnouncer().getAnnouncements());

            //save and reload announcements config
            plugin.getConfigManager().saveAnnouncer();
            plugin.getConfigManager().reloadAnnouncer();

            //reload announcer
            plugin.getAnnouncer().reload();

            MessageManager.sendPrefixMessage(PrefixType.MAIN, "Announcement added successfully!", sender);
        }

        if (args[0].equalsIgnoreCase("remove")) {
            if (args.length == 1) {
                throw new CommandException("Usage: /announcer remove <index>");
            }

            int announcementIndex;

            try {
                announcementIndex = parseInt(args[1]);
            } catch (Exception ex) {
                throw new CommandException("invalid announcement index, the announcements array starts at 0.");
            }

            //remove the announcement
            plugin.getAnnouncer().getAnnouncements().remove(announcementIndex);
            ConfigManager.getAnnouncer().set("announcements", plugin.getAnnouncer().getAnnouncements());

            //save and reload announcements config
            plugin.getConfigManager().saveAnnouncer();
            plugin.getConfigManager().reloadAnnouncer();

            //reload announcer
            plugin.getAnnouncer().reload();

            MessageManager.sendPrefixMessage(PrefixType.MAIN, "Announcement removed successfully!", sender);
        }

        if (args[0].equalsIgnoreCase("list")) {
            StringBuilder list = new StringBuilder("");
            int i = 1;

            for (String announcement : plugin.getAnnouncer().getAnnouncements()) {
                if (list.length() > 0) {
                    if (i == plugin.getAnnouncer().getAnnouncements().size()) {
                        list.append("§8 and §7");
                    } else {
                        list.append("§8, §7");
                    }
                }

                list.append(announcement);
                i++;
            }

            MessageManager.sendPrefixMessage(PrefixType.MAIN, "List of all announcements: §8(§6" + plugin.getAnnouncer().getAnnouncements().size() + "§8)", sender);

            MessageManager.sendPrefixMessage(PrefixType.ARROW, list.toString().trim(), sender);
        }

        if (args[0].equalsIgnoreCase("enabled")) {

            if (args.length == 1) {
                throw new CommandException("Usage: /announcer enabled <true|false>");
            }

            boolean enabled;

            try {
                enabled = parseBoolean(args[1]);
            } catch (Exception e) {
                throw new CommandException("Invalid option provided, must be true or false.");
            }

            ConfigManager.getAnnouncer().set("enabled", enabled);

            //save and reload announcements config
            plugin.getConfigManager().saveAnnouncer();
            plugin.getConfigManager().reloadAnnouncer();

            //reload announcer
            plugin.getAnnouncer().reload();

            if (enabled) {
                MessageManager.sendPrefixMessage(PrefixType.MAIN, "Announcer enabled successfully!", sender);
            } else {
                MessageManager.sendPrefixMessage(PrefixType.MAIN, "Announcer disabled successfully!", sender);
            }

        }

        return true;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String[] args) {
        List<String> toReturn = new ArrayList<String>();

        if (args.length == 1) {
            toReturn.add("add");
            toReturn.add("remove");
            toReturn.add("list");
            toReturn.add("enabled");
        }

        if (args.length == 2) {
            if (args[0].equalsIgnoreCase("enabled")) {
                toReturn.add("true");
                toReturn.add("false");
            }
        }

        return toReturn;
    }

}
