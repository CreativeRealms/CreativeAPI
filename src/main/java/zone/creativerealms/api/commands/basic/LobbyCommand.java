package zone.creativerealms.api.commands.basic;

import org.bukkit.command.CommandException;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import zone.creativerealms.api.commands.AbstractCommand;
import zone.creativerealms.api.config.ConfigManager;
import zone.creativerealms.api.message.MessageManager;
import zone.creativerealms.api.message.PrefixType;
import zone.creativerealms.api.user.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) CreativeRealms - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: zone.creativerealms.api
 * Project: CreativeAPI
 */
public class LobbyCommand extends AbstractCommand {

	public LobbyCommand() {
		super("lobby", "<set|enable|disable>");
	}

	@Override
	public boolean execute(final CommandSender sender, final String[] args) throws CommandException {

		if (!(sender instanceof Player)) {
			throw new CommandException("Only players can set the lobby.");
		}

		Player player = (Player) sender;
		User user = plugin.getUserManager().getUser(player);

		if (args[0].equalsIgnoreCase("set")) {

			//set the lobby
			ConfigManager.getLobby().set("location", user.getPlayer().getLocation().toVector());
			ConfigManager.getLobby().set("set", true);
			plugin.getConfigManager().saveLobby();

			//reload the lobby config
			plugin.getConfigManager().reloadLobby();

			MessageManager.sendPrefixMessage(PrefixType.MAIN, "Lobby spawn set to current location.", player);
		}

		if (args[0].equalsIgnoreCase("enable")) {

			//enable the lobby
			ConfigManager.getLobby().set("enabled", true);
			plugin.getConfigManager().saveLobby();

			//reload the lobby config
			plugin.getConfigManager().reloadLobby();

			MessageManager.sendPrefixMessage(PrefixType.MAIN, "Lobby successfully enabled!", player);
		}

		if (args[0].equalsIgnoreCase("disable")) {

			//enable the lobby
			ConfigManager.getLobby().set("disable", false);
			plugin.getConfigManager().saveLobby();

			//reload the lobby config
			plugin.getConfigManager().reloadLobby();

			MessageManager.sendPrefixMessage(PrefixType.MAIN, "Lobby successfully disabled!", player);

		}

		return true;
	}

	@Override
	public List<String> tabComplete(CommandSender sender, String[] args) {
		List<String> toReturn = new ArrayList<>();

		if (args.length == 1) {
			toReturn.add("set");
			toReturn.add("enable");
			toReturn.add("disable");
		}

		return toReturn;
	}
}