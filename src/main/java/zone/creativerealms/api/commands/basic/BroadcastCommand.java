package zone.creativerealms.api.commands.basic;

import com.google.common.base.Joiner;
import zone.creativerealms.api.commands.AbstractCommand;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.boss.BarColor;
import org.bukkit.command.CommandSender;
import zone.creativerealms.api.message.MessageManager;
import zone.creativerealms.api.message.PrefixType;

import java.util.Arrays;
import java.util.List;

/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) CreativeRealms - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: zone.creativerealms.api
 * Project: CreativeAPI
 */
public class BroadcastCommand extends AbstractCommand {

    public BroadcastCommand() {
        super("broadcast", "<message>");
    }

    @Override
    public boolean execute(final CommandSender sender, final String[] args) {
        if (args.length == 0) {
            return false;
        }

        String message = Joiner.on(' ').join(Arrays.copyOfRange(args, 0, args.length));

        //broadcast the message
        MessageManager.broadcastBarPrefix(PrefixType.MAIN, BarColor.BLUE, ChatColor.translateAlternateColorCodes('&', message));
        MessageManager.broadcastSound(Sound.BLOCK_NOTE_BASS);

        return true;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String[] args) {
        return allPlayers();
    }

}
