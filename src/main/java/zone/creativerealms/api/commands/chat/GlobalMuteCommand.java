package zone.creativerealms.api.commands.chat;

import zone.creativerealms.api.commands.AbstractCommand;
import zone.creativerealms.api.user.User;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) CreativeRealms - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: zone.creativerealms.api
 * Project: CreativeAPI
 */
public class GlobalMuteCommand extends AbstractCommand {

	public GlobalMuteCommand() {
		super("globalmute", "");
	}

	@Override
	public boolean execute(final CommandSender sender, final String[] args) throws CommandException {

		if (!(sender instanceof Player)) {
			throw new CommandException("Only players can global mute.");
		}

		Player player = (Player) sender;
		User user = plugin.getUserManager().getUser(player);

		if (user.hasGroupPerm("creativerealms.chat.globalmute")) {
			plugin.getChatListener().toggleChat();
		} else {
			throw new CommandException("You can not enable global mute");
		}

		return true;
	}

	@Override
	public List<String> tabComplete(CommandSender sender, String[] args) {
		return new ArrayList<>();
	}
}