package zone.creativerealms.api.listeners;

import zone.creativerealms.api.CreativeAPI;
import zone.creativerealms.api.user.User;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;

/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) CreativeRealms - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: zone.creativerealms.api
 * Project: CreativeAPI
 */
public class WorldListener implements Listener {

    //CreativeAPI plugin instance
    private CreativeAPI plugin;

    public WorldListener(CreativeAPI plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {

        Player player = e.getPlayer();

        User user = plugin.getUserManager().getUserMap().get(player.getUniqueId());

        if (player.getWorld() == plugin.getServer().getWorld("lobby")) {
            if (!user.hasGroupPerm("creativerealms.bypass.protection")) {
                e.setCancelled(true);
            }
        }

        if (player.getWorld() == plugin.getServer().getWorld("plots")) {
            player.setGameMode(GameMode.CREATIVE);
        }

    }

    @EventHandler
    public void onWorldChanged(PlayerChangedWorldEvent e) {

        Player player = e.getPlayer();

        if (player.getWorld() == plugin.getServer().getWorld("lobby")) {
            player.setGameMode(GameMode.ADVENTURE);
        }

    }


}
