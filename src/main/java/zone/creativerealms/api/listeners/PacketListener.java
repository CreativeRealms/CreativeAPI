package zone.creativerealms.api.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;

/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) CreativeRealms - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: zone.creativerealms.api
 * Project: CreativeAPI
 */
public class PacketListener implements Listener {

    private static PacketListener instance;

    public static PacketListener getInstance() {
        return instance;
    }


    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if(!channel.equals("CreativeAPI")) {
            return;
        }
        try {
            DataInputStream msgin = new DataInputStream(new ByteArrayInputStream(message));
            String subchannel = msgin.readUTF();
            if(subchannel.equals("Chat")) {
                String chatchannel = msgin.readUTF();
                String prefix = msgin.readUTF();
                String playerName = msgin.readUTF();
                String chatMessage = msgin.readUTF();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

}
