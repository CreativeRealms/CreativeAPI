package zone.creativerealms.api.listeners;

import org.bukkit.entity.Player;
import zone.creativerealms.api.CreativeAPI;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import zone.creativerealms.api.utils.communication.ChatUtils;

/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) CreativeRealms - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: zone.creativerealms.api
 * Project: CreativeAPI
 */
public class JoinListener implements Listener {

    private CreativeAPI plugin;

    public JoinListener(CreativeAPI plugin) {
        this.plugin = plugin;
    }

    private final String login_header =
            ChatColor.DARK_AQUA + "Creative" + ChatColor.DARK_PURPLE + "Realms\n                    "
                    + ChatColor.GRAY + ">" + ChatColor.STRIKETHROUGH + ChatUtils.repeat(100, " ") + ChatColor.GRAY + "<"
                    + "\n\n" + ChatColor.RED;
    private final String login_footer =
            "\n\n" + ChatColor.GRAY + ">" + ChatColor.STRIKETHROUGH + ChatUtils.repeat(100, " ") + ChatColor.GRAY + "<"
                    + "\n" + ChatColor.GREEN + ChatColor.ITALIC.toString() + "@CreativeRealmsMC";

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        event.setJoinMessage(null);

        plugin.getUserManager().procesJoin(player);

    }

    @EventHandler
    public void onDisconnect(PlayerQuitEvent event) {
        Player player = event.getPlayer();

        plugin.getUserManager().processLeave(player);
    }

    @EventHandler
    public void onKick(PlayerKickEvent event) {
        plugin.getUserManager().processLeave(event.getPlayer());
    }

}
