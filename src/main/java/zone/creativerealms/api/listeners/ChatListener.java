package zone.creativerealms.api.listeners;

import zone.creativerealms.api.CreativeAPI;
import zone.creativerealms.api.config.ConfigManager;
import zone.creativerealms.api.message.MessageManager;
import zone.creativerealms.api.message.PrefixType;
import zone.creativerealms.api.permissions.PermissionManager;
import zone.creativerealms.api.player.CreativePlayer;
import zone.creativerealms.api.player.PlayerManager;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Sound;
import org.bukkit.boss.BarColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import zone.creativerealms.api.player.Rank;


/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) CreativeRealms - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: zone.creativerealms.api
 * Project: CreativeAPI
 */
public class ChatListener implements Listener {

    private CreativeAPI plugin;
    private boolean chatEnabled;

    public ChatListener(CreativeAPI plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        CreativePlayer creativePlayer = PlayerManager.getCreativePlayer(player.getUniqueId());

        if (creativePlayer == null) {
            event.setFormat(player.getName() + " " + ChatColor.DARK_GRAY + "» " + ChatColor.RESET + event.getMessage());
            return;
        }

        if (!isChatEnabled()) {
            if (!PermissionManager.isStaff(creativePlayer)) {
                event.setCancelled(true);
                player.sendMessage("Global chat is currently disabled.");
            }
        }

        if (PermissionManager.checkRankSilent(creativePlayer, Rank.VIP)) {
            event.setFormat(creativePlayer.getPrefix() + ChatColor.RESET + creativePlayer.getNameColor() +  creativePlayer.getName() + " " + ChatColor.DARK_GRAY + "» " + ChatColor.RESET + event.getMessage());
        } else {
            event.setFormat(creativePlayer.getPrefix() + ChatColor.RESET + creativePlayer.getNameColor() + creativePlayer.getName() + " " + ChatColor.DARK_GRAY + "» " + ChatColor.RESET + event.getMessage());
        }

    }

    public void setChatEnabled(boolean enabled) {
        chatEnabled = enabled;

        //update config
        ConfigManager.getConfig().set("chat.enabled", enabled);
        plugin.getConfigManager().saveConfig();
    }

    public boolean isChatEnabled() {
        return chatEnabled;
    }

    public void toggleChat(){
        if (isChatEnabled()) {
            muteChat();
        } else {
            unmuteChat();
        }
    }

    public void muteChat() {
        setChatEnabled(false);

        MessageManager.broadcastBarPrefix(PrefixType.MAIN, BarColor.BLUE, ChatColor.BLUE + "Global mute enabled.");
        MessageManager.broadcastSound(Sound.BLOCK_NOTE_BASS);
        //PlayerUtils.broadcast(ChatColor.BLUE + "Global mute enabled.");
    }

    public void unmuteChat() {
        setChatEnabled(true);

        MessageManager.broadcastBarPrefix(PrefixType.MAIN, BarColor.BLUE, ChatColor.BLUE + "Global mute disabled.");
        MessageManager.broadcastSound(Sound.BLOCK_NOTE_BASS);
        //PlayerUtils.broadcast(ChatColor.BLUE + "Global mute disabled.");
    }

}
