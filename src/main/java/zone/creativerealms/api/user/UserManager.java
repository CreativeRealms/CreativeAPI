package zone.creativerealms.api.user;

import zone.creativerealms.api.CreativeAPI;
import org.bukkit.entity.Player;
import zone.creativerealms.api.message.MessageManager;

import java.util.Collection;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) CreativeRealms - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: zone.creativerealms.api
 * Project: CreativeAPI
 */
public class UserManager {

    private CreativeAPI plugin;

    private ConcurrentMap<UUID, User> userMap = new ConcurrentHashMap<>();

    public UserManager(CreativeAPI plugin) {
        this.plugin = plugin;
    }

    public ConcurrentMap<UUID, User> getUserMap() {
        return userMap;
    }

    public Collection<User> getUsers(){
        return userMap.values();
    }

    public User getUser(Player player) {
        return  player == null ? null : userMap.get(player.getUniqueId());
    }

    public User procesJoin(Player p){
        User user = getUserMap().get(p.getUniqueId());

        if(user == null){
            user = new User(p.getUniqueId());
            getUserMap().put(p.getUniqueId(), user);
        }

        user.setPlayer(p);
        user.setPermissionUser(user.getPermissionUser());
        MessageManager.getAnnounceBar().addPlayer(p);
        return user;
    }

    public void processLeave(Player p) {

        //remove the user from the UserMap and the AnnounceBar
        getUserMap().remove(p.getUniqueId());
        MessageManager.getAnnounceBar().removePlayer(p);

    }

}
