package zone.creativerealms.api.permissions;

import com.google.common.collect.ImmutableMap;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;
import zone.creativerealms.api.CreativeAPI;
import zone.creativerealms.api.message.MessageManager;
import zone.creativerealms.api.message.PrefixType;
import zone.creativerealms.api.player.PlayerManager;
import zone.creativerealms.api.player.Rank;
import zone.creativerealms.api.player.CreativePlayer;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) CreativeRealms - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: zone.creativerealms.api
 * Project: CreativeAPI
 */
public class PermissionManager {

    private static final Map<UUID, PermissionAttachment> perms = new HashMap<UUID, PermissionAttachment>();

    /**
     * Get a copy of the permissions HashMap.
     *
     * @return Copy of permissions HashMap.
     */
    public static Map<UUID, PermissionAttachment> getPerms() {
        return ImmutableMap.copyOf(perms);
    }

    public static boolean checkRank(CreativePlayer creativePlayer, Rank rank) {
        if (creativePlayer.getRank().getLevel() >= rank.getLevel()) {
            return true;
        }

        creativePlayer.getPlayer().sendMessage(MessageManager.getPrefix(PrefixType.PERMISSIONS) + "This requires Permission Rank " + rank.getTag());

        return false;
    }

    public static boolean checkRankSilent(CreativePlayer creativePlayer, Rank rank) {
        if (creativePlayer.getRank().getLevel() >= rank.getLevel()) {
            return true;
        }

        return false;
    }

    public static boolean isStaff(CreativePlayer creativePlayer) {
        if (creativePlayer.getRank().getLevel() >= 7) {
            return true;
        }

        return false;
    }

    public static void addPerms(CreativePlayer creativePlayer) {
        if (!perms.containsKey(creativePlayer.getUUID())) {
            perms.put(creativePlayer.getUUID(), creativePlayer.getPlayer().addAttachment(CreativeAPI.getInstance()));
        }

        PermissionAttachment perm = perms.get(creativePlayer.getUUID());
        Rank rank = Rank.DEFAULT;

        rank = creativePlayer.getRank();

        if (rank == Rank.OWNER) {
            perm.setPermission("*", true);
            return;
        }

        if (rank == Rank.ADMIN) {
            perm.setPermission("creativerealms.*", true);
            return;
        }
    }

    /**
     * Handle the permissions for the given player if he leaves.
     *
     * @param creativePlayer the player.
     */
    public static void removePerms(CreativePlayer creativePlayer) {
        if (!perms.containsKey(creativePlayer.getUUID())) {
            return;
        }

        try {
            creativePlayer.getPlayer().removeAttachment(perms.get(creativePlayer.getUUID()));
        } catch (Exception e) {
            Bukkit.getLogger().warning("Couldn't remove " + creativePlayer.getName() + "'s permissions.");
            Bukkit.getLogger().warning(e.getClass().getName() + ": " + e.getMessage());
        }

        perms.remove(creativePlayer.getUUID());
    }

}
