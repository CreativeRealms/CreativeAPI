package zone.creativerealms.api.server;

import zone.creativerealms.api.CreativeAPI;
import zone.creativerealms.api.server.creative.CreativeServer;
import zone.creativerealms.api.server.lobby.LobbyServer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import org.jooq.DSLContext;

import static zone.creativerealms.database.Tables.LOBBY_SERVERS;

import static org.jooq.impl.DSL.row;
import static org.jooq.impl.DSL.table;

/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) CreativeRealms - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: zone.creativerealms.api
 * Project: CreativeAPI
 */
public class ServerManager {

    private static ServerManager instance;
    private List<LobbyServer> lobbyServers = new ArrayList<>();
    private List<CreativeServer> creativeServers = new ArrayList<>();

    public static  ServerManager getInstance() {
        if (instance == null) {
            instance = new ServerManager();
        }
        return instance;
    }

    public List<LobbyServer> getLobbyServers() {
        return lobbyServers;
    }

    public List<CreativeServer> getCreativeServers() {
        return creativeServers;
    }

    public void updateLobbyServer(LobbyServer lobbyServer, String key, Object value) {
        try {
            PreparedStatement stmt = CreativeAPI.getDB().getConnection().prepareStatement("UPDATE lobby_servers SET " + key + " = ? WHERE uuid = ?;");
            stmt.setObject(1, value);
            stmt.setString(2, lobbyServer.getUUID().toString());
            stmt.execute();
            stmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void updateCreativeServer(CreativeServer creativeServer, String key, Object value) {
        try {
            PreparedStatement stmt = CreativeAPI.getDB().getConnection().prepareStatement("UPDATE creative_servers SET " + key + " = ? WHERE uuid = ?;");
            stmt.setObject(1, value);
            stmt.setString(2, creativeServer.getUUID().toString());
            stmt.execute();
            stmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public LobbyServer getSQLLobbyServer(UUID uuid) {
        try {
            PreparedStatement stmt = CreativeAPI.getDB().getConnection().prepareStatement("SELECT * FROM lobby_servers WHERE uuid = ? LIMIT 1;");
            stmt.setString(1, uuid.toString());
            ResultSet set = stmt.executeQuery();
            if (set.next()) {

                LobbyServer server = new LobbyServer(uuid);
                server.setName(set.getString("name"));
                server.setAddress(set.getString("address"));
                server.setRegion(Region.valueOf(set.getString("region")));
                server.setState(ServerState.valueOf(set.getString("state")));
                server.setCount(set.getInt("count"));
                server.setMax(set.getInt("max"));

                return server;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public CreativeServer getSQLCreativeServer(UUID uuid) {
        try {

            PreparedStatement stmt = CreativeAPI.getDB().getConnection().prepareStatement("SELECT * FROM creative_servers WHERE uuid = ? LIMIT 1");
            stmt.setString(1, uuid.toString());
            ResultSet set = stmt.executeQuery();
            if (set.next()) {

                CreativeServer server = new CreativeServer(uuid);
                server.setName(set.getString("name"));
                server.setAddress(set.getString("address"));
                server.setRegion(Region.valueOf(set.getString("region")));
                server.setState(ServerState.valueOf(set.getString("state")));
                server.setCount(set.getInt("count"));
                server.setMax(set.getInt("max"));
                server.setPlotType(PlotType.valueOf(set.getString("plot_type")));

                return server;

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public void globalLobbyServerSync(LobbyServer server) {

        boolean newServer;
        String uuid = server.getUUID().toString();


        try {
            PreparedStatement stmt =  CreativeAPI.getDB().getConnection().prepareStatement("SELECT * FROM lobby_servers WHERE uuid = ? LIMIT 1;");
            stmt.setString(1, server.getUUID().toString());
            ResultSet set = stmt.executeQuery();
            if (set.next()) {
                stmt.close();

                //update name
                updateLobbyServer(server, "name", server.getName());
                /*stmt = CreativeAPI.getDB().getConnection().prepareStatement("UPDATE lobby_servers SET name = ? WHERE uuid = ?;");
                stmt.setString(1, server.getName());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();*/

                //update address
                stmt = CreativeAPI.getDB().getConnection().prepareStatement("UPDATE lobby_servers SET address = ? WHERE uuid = ?;");
                stmt.setString(1, server.getAddress());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

                //update region
                stmt = CreativeAPI.getDB().getConnection().prepareStatement("UPDATE lobby_servers SET region = ? WHERE uuid = ?;");
                stmt.setString(1, server.getRegion().toString());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

                //update state
                stmt = CreativeAPI.getDB().getConnection().prepareStatement("UPDATE lobby_servers SET state = ? WHERE uuid = ?;");
                stmt.setString(1, server.getState().toString());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

                //update count
                stmt = CreativeAPI.getDB().getConnection().prepareStatement("UPDATE lobby_servers SET count = ? WHERE uuid = ?;");
                stmt.setInt(1, server.getCount());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

                //update max
                stmt = CreativeAPI.getDB().getConnection().prepareStatement("UPDATE lobby_servers SET max = ? WHERE uuid = ?;");
                stmt.setInt(1, server.getMax());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

            } else {

                stmt.close();
                stmt = CreativeAPI.getDB().getConnection().prepareStatement("INSERT INTO lobby_servers (uuid) VALUES (?);");
                stmt.setString(1, uuid);
                stmt.execute();
                stmt.close();

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void globalCreativeServerSync(CreativeServer server) {

        String uuid = server.getUUID().toString();

        try {
            PreparedStatement stmt = CreativeAPI.getDB().getConnection().prepareStatement("SELECT * FROM creative_servers WHERE uuid = ? LIMIT 1;");
            stmt.setString(1, server.getUUID().toString());
            ResultSet set = stmt.executeQuery();
            if (set.next()) {
                stmt.close();

                //update name
                stmt = CreativeAPI.getDB().getConnection().prepareStatement("UPDATE creative_servers SET name = ? WHERE uuid = ?;");
                stmt.setString(1, server.getName());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

                //update address
                stmt = CreativeAPI.getDB().getConnection().prepareStatement("UPDATE creative_servers SET address = ? WHERE uuid = ?;");
                stmt.setString(1, server.getAddress());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

                //update region
                stmt = CreativeAPI.getDB().getConnection().prepareStatement("UPDATE creative_servers SET region = ? WHERE uuid = ?;");
                stmt.setString(1, server.getRegion().toString());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

                //update state
                stmt = CreativeAPI.getDB().getConnection().prepareStatement("UPDATE creative_servers SET state = ? WHERE uuid = ?;");
                stmt.setString(1, server.getState().toString());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

                //update count
                stmt = CreativeAPI.getDB().getConnection().prepareStatement("UPDATE creative_servers SET count = ? WHERE uuid = ?;");
                stmt.setInt(1, server.getCount());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

                //update max
                stmt = CreativeAPI.getDB().getConnection().prepareStatement("UPDATE creative_servers SET max = ? WHERE uuid = ?;");
                stmt.setInt(1, server.getMax());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

                //update plot type
                stmt = CreativeAPI.getDB().getConnection().prepareStatement("UPDATE creative_servers SET plot_type = ? WHERE uuid = ?;");
                stmt.setString(1, server.getPlotType().toString());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

            } else {

                stmt.close();
                stmt = CreativeAPI.getDB().getConnection().prepareStatement("INSERT INTO creative_servers (uuid) VALUES (?);");
                stmt.setString(1, uuid);
                stmt.execute();
                stmt.close();

            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }


    public LobbyServer getLobbyServer(UUID uuid) {
        for (LobbyServer server : lobbyServers) {
            if (server.getUUID().equals(uuid)) {
                return server;
            }
        }

        return getSQLLobbyServer(uuid);
    }

    public CreativeServer getCreativeServer(UUID uuid) {
        for (CreativeServer server : creativeServers) {
            if (server.getUUID().equals(uuid)) {
                return server;
            }
        }
        return getSQLCreativeServer(uuid);
    }

    public void loadLobbyServers() {

        try {
            List<UUID> downloadedServers = new ArrayList<>();
            PreparedStatement stmt = CreativeAPI.getDB().getConnection().prepareStatement("SELECT uuid FROM lobby_servers;");
            ResultSet set = stmt.executeQuery();
            while (set.next()) {
                UUID uuid = UUID.fromString(set.getString("uuid"));
                downloadedServers.add(uuid);
                LobbyServer server = getLobbyServer(uuid);
                if (!lobbyServers.contains(server)) {
                    lobbyServers.add(server);
                }
            }
            set.close();
            stmt.close();
            if (downloadedServers.size() > 0) {
                for (int i = 0; i < getLobbyServers().size(); i++) {
                    LobbyServer server = getLobbyServers().get(i);
                    if (!downloadedServers.contains(server.getUUID())) {
                        getLobbyServers().remove(server);
                    }
                }
            }
            for (LobbyServer server : getLobbyServers()) {
                stmt = CreativeAPI.getDB().getConnection().prepareStatement("SELECT * FROM lobby_servers WHERE uuid = ? LIMIT 1;");
                stmt.setString(1, server.getUUID().toString());
                set = stmt.executeQuery();
                if (set.next()) {
                    server.setName(set.getString("name"));
                    server.setAddress(set.getString("address"));
                    server.setRegion(Region.valueOf(set.getString("region")));
                    server.setState(ServerState.valueOf(set.getString("state")));
                    server.setCount(set.getInt("count"));
                    server.setMax(set.getInt("max"));
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void loadCreativeServers() {

        try {
            List<UUID> downloadedServers = new ArrayList<>();
            PreparedStatement stmt = CreativeAPI.getDB().getConnection().prepareStatement("SELECT uuid FROM creative_servers;");
            ResultSet set = stmt.executeQuery();
            while (set.next()) {
                UUID uuid = UUID.fromString(set.getString("uuid"));
                downloadedServers.add(uuid);
                CreativeServer server = getCreativeServer(uuid);
                if (!creativeServers.contains(server)) {
                    creativeServers.add(server);
                }
            }
            set.close();
            stmt.close();
            if (downloadedServers.size() > 0) {
                for (int i = 0; i < creativeServers.size(); i++) {
                    CreativeServer server = creativeServers.get(i);
                    if (!downloadedServers.contains(server.getUUID())) {
                        creativeServers.remove(server);
                    }
                }
            }
            for (CreativeServer server : creativeServers) {
                stmt = CreativeAPI.getDB().getConnection().prepareStatement("SELECT * FROM creative_servers WHERE uuid = ? LIMIT 1;");
                stmt.setString(1, server.getUUID().toString());
                set = stmt.executeQuery();
                if (set.next()) {
                    server.setName(set.getString("name"));
                    server.setAddress(set.getString("address"));
                    server.setRegion(Region.valueOf(set.getString("region")));
                    server.setState(ServerState.valueOf(set.getString("state")));
                    server.setCount(set.getInt("count"));
                    server.setMax(set.getInt("max"));
                    server.setPlotType(PlotType.valueOf(set.getString("plot_type")));
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


}
