package zone.creativerealms.api.server.creative;

import zone.creativerealms.api.server.PlotType;
import zone.creativerealms.api.server.Region;
import zone.creativerealms.api.server.ServerState;

import java.util.UUID;

/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) CreativeRealms - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: zone.creativerealms.api
 * Project: CreativeAPI
 */
public class CreativeServer {

    //server settings
    private UUID uuid;
    private String name;
    private String address;
    private long lastHeartbeat = 0;
    private ServerState state;
    private Region region;
    private int count;
    private int max;

    //creative specific settings
    private PlotType plotType;

    public CreativeServer(UUID uuid) {
        this.uuid = uuid;
    }

    //getters
    public UUID getUUID() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public long getLastHeartbeat() {
        return lastHeartbeat;
    }

    public ServerState getState() {
        return state;
    }

    public Region getRegion() {
        return region;
    }

    public int getCount() {
        return count;
    }

    public int getMax() {
        return max;
    }

    public PlotType getPlotType() {
        return plotType;
    }

    //setters
    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setLastHeartbeat(long lastHeartbeat) {
        this.lastHeartbeat = lastHeartbeat;
    }

    public void setState(ServerState state) {
        this.state = state;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public void setPlotType(PlotType type) {
        this.plotType = type;
    }

}
